<?php


// Function to fetch user profile data
function getUserProfile($userId) {

    // Return the results as JSON
}

// Function to fetch user's posts
function getUserPosts($userId) {

    // Return the results as JSON
}

// Check the requested action 
if (isset($_GET['action'])) {
    if ($_GET['action'] === 'getUserProfile' && isset($_GET['user_id'])) {
        echo json_encode(getUserProfile($_GET['user_id']));
    } elseif ($_GET['action'] === 'getUserPosts' && isset($_GET['user_id'])) {
        echo json_encode(getUserPosts($_GET['user_id']));
    }
} else {
    echo json_encode(['error' => 'Invalid action']);
}
