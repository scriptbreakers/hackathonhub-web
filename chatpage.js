document.addEventListener('DOMContentLoaded', function () {
    
    const chatRoomsApiUrl = 'http://localhost:'; // Replace with API endpoint pzl

    
    const chatMessagesApiUrl = 'http://localhost:'; // Replace with actual API endpoint pzl

    const chatRoomsList = document.getElementById('chat-rooms-list');
    const chatMessagesContainer = document.querySelector('.chat-messages');
    const messageInput = document.getElementById('message-input');
    const sendButton = document.getElementById('send-button');

    // Fetch chat rooms from the server
    fetch(chatRoomsApiUrl)
        .then(response => response.json())
        .then(chatRooms => {
            
            chatRooms.forEach(chatRoom => {
                const roomItem = document.createElement('li');
                roomItem.textContent = chatRoom.name;
                roomItem.dataset.id = chatRoom.id;
                chatRoomsList.appendChild(roomItem);
            });

            // Add event listener to chat room items for loading messages
            chatRoomsList.addEventListener('click', function (event) {
                const roomItem = event.target;
                if (roomItem.tagName === 'LI') {
                    loadChatMessages(roomItem.dataset.id);
                }
            });
        })
        .catch(error => {
            console.error('Error fetching chat rooms:', error);
        });

    // Function to load chat messages for a specific room
    function loadChatMessages(roomId) {
        // Fetch chat messages for the specified room
        fetch(`${chatMessagesApiUrl}?room_id=${roomId}`)
            .then(response => response.json())
            .then(chatMessages => {
                // Clear existing messages
                chatMessagesContainer.innerHTML = '';

                // Iterate through chat messages and append them to the container
                chatMessages.forEach(message => {
                    const messageItem = document.createElement('div');
                    messageItem.textContent = message.text;
                    chatMessagesContainer.appendChild(messageItem);
                });
            })
            .catch(error => {
                console.error('Error fetching chat messages:', error);
            });
    }

    // Add event listener to send messages
    sendButton.addEventListener('click', function () {
        const roomId = /* Get the selected room's ID */:
        const messageText = messageInput.value;
        
    });
});
