<?php
// Database connection
$host = "your_database_host";
$username = "your_database_username";
$password = "your_database_password";
$database = "your_database_name";

$connection = new mysqli($host, $username, $password, $database);

if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
}

// Handle form data
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST["register"])) {
        // Registration form submitted
        $username = $_POST["username"];
        $email = $_POST["email"];
        $password = $_POST["password"];

        // Insert user data into the database
        $query = INSERT INTO `user`(`UserID`, `Email`, `Password`, `Username`, `PhotoUrl`) VALUES ('[value-1]','[value-2]','[value-3]','[value-4]','[value-5]')

        if ($connection->query($query) === TRUE) {
            echo "Signup successful!";
        } else {
            echo "Error: " . $query . "<br>" . $connection->error;
        }
    } elseif (isset($_POST["login"])) {
        // Login form submitted
        $loginEmail = $_POST["loginEmail"];
        $loginPassword = $_POST["loginPassword"];

        // check if the login credentials are valid
        $query = "SELECT * FROM users WHERE email='$loginEmail' AND password='$loginPassword'";
        $result = $connection->query($query);

        if ($result->num_rows == 1) {
           
            echo "Login successful!";
        } else {
          
            echo "Login failed. Check your email and password.";
        }
    }
}

$connection->close();
?>
