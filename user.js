document.addEventListener('DOMContentLoaded', function () {
    
    const userProfileApiUrl = 'http://localhost:'; // Replace with API endpoint

    const profileHeader = document.querySelector('.profile-header');
    const profilePostsContainer = document.getElementById('profile-posts');

    // Fetch user profile data from the server
    fetch(userProfileApiUrl)
        .then(response => response.json())
        .then(userProfileData => {
            // Populate the profile header with user data
            populateUserProfileHeader(userProfileData);

            // Fetch and populate user's posts
            fetchUserPosts(userProfileData.id);
        })
        .catch(error => {
            console.error('Error fetching user profile data:', error);
        });

    // Function to populate the profile header with user data
    function populateUserProfileHeader(userProfileData) {
        const profileUsername = document.createElement('div');
        profileUsername.textContent = userProfileData.username;
        profileHeader.appendChild(profileUsername);

        // Populate other user details 
    }

    // Function to fetch and populate user's posts
    function fetchUserPosts(userId) {

        const userPostsApiUrl = `http://localhost:port/api/user_posts?user_id=${userId}`; // Replace actual API endpoint pplz

        fetch(userPostsApiUrl)
            .then(response => response.json())
            .then(userPosts => {
                // Iterate through user's posts and append them to the container
                userPosts.forEach(post => {
                    const postItem = document.createElement('div');
                    postItem.textContent = post.text;
                    profilePostsContainer.appendChild(postItem);
                });
            })
            .catch(error => {
                console.error('Error fetching user posts:', error);
            });
    }

    // Add event listeners and functionality for the modal and post creation
});
