


const wrapper = document.querySelector(".wrapper"),
          signupHeader = document.querySelector(".signup header"),
          loginHeader = document.querySelector(".login header");
        loginHeader.addEventListener("click", () => {
          wrapper.classList.add("active");
        });
        signupHeader.addEventListener("click", () => {
          wrapper.classList.remove("active");
        });

document.addEventListener('DOMContentLoaded', function () {
    const loginForm = document.getElementById('loginForm');
  
    loginForm.addEventListener('submit', async function (e) {
        e.preventDefault();
  
        const Email = document.getElementById('loginEmail').value;
        const Password = document.getElementById('loginPassword').value;
  
        try {
            // Send login data to the API endpoint
            const response = await fetch('http://192.168.157.172:4000/api/users/login', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ Email, Password })
            });
  
            const data = await response.json();
  
            if (response.status === 201) {
                alert('Login successful');
                const token = data.token;
              
                localStorage.setItem('token', token); 
                // Store the JWT token (data.token) in localStorage or a cookie
                // Redirect to the dashboard or perform any desired action
                window.location.href = 'index.html'
  
            } else {
                alert('Login failed: ' + data.error);
            }
        } catch (error) {
            console.error('Error during login:', error);
        }
    });
  });
  
  
  
  document.addEventListener('DOMContentLoaded', function () {
      const registrationForm = document.getElementById('registrationForm');
  
      registrationForm.addEventListener('submit', async function (e) {
          e.preventDefault();
  
          const Username = document.getElementById('username').value;
          const Email = document.getElementById('email').value;
          const Password = document.getElementById('password').value;
          console.log(Username + Email + Password);
  
          try {
              // Send registration data to the API endpoint
              const response = await fetch('http://192.168.157.172:4000/api/users/register', {
                  method: 'POST',
                  headers: {
                      'Content-Type': 'application/json'
                  },
                  body: JSON.stringify({ Username, Email, Password })
              });
  
              const data = await response.json();
              
  
              if (response.status === 201) {
                  alert('Registration successful');
                  wrapper.classList.add("active");// Redirect to the login page
              } else {
                  alert('Registration failed: ' + data.error);
              }
          } catch (error) {
              console.error('Error during registration:', error);
          }
      });
  });
