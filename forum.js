document.addEventListener('DOMContentLoaded', function () {
    // Your API endpoint for fetching discussions
    const discussionsApiUrl = 'http://localhost:3306/discussions_api.php'; // Replace with your actual API endpoint

    const discussionsList = document.getElementById('discussions-list');

    // Fetch discussions from the server
    fetch(discussionsApiUrl)
        .then(response => response.json())
        .then(data => {
            // Iterate through the discussions and populate the list
            data.forEach(discussion => {
                const discussionItem = document.createElement('li');
                discussionItem.innerHTML = `<a href="discussion.php?id=${discussion.id}">${discussion.title}</a>`;
                discussionsList.appendChild(discussionItem);
            });
        })
        .catch(error => {
            console.error('Error fetching discussions:', error);
        });
});
