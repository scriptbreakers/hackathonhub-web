<?php

function getChatRooms() {
    
    // Return the results as JSON
}

// Function to fetch chat messages for a specific room
function getChatMessages($roomId) {
   
    // Return the results as JSON
}

// Check the requested action
if (isset($_GET['action'])) {
    if ($_GET['action'] === 'getChatRooms') {
        echo json_encode(getChatRooms());
    } elseif ($_GET['action'] === 'getChatMessages' && isset($_GET['room_id'])) {
        echo json_encode(getChatMessages($_GET['room_id']));
    }
} else {
    echo json_encode(['error' => 'Invalid action']);
}
